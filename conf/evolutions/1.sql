
# --- !Ups
CREATE TABLE `Question` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`title`	TEXT
);
# --- !Downs
DROP TABLE Question;