# --- !Ups

CREATE TABLE `Question` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`title`	TEXT,
	`detail`	TEXT,
	`date`	NUMERIC,
	`tags`	TEXT,
	`userId`	TEXT,
	`totalAnswers`	INTEGER NOT NULL
);

CREATE TABLE `Answer` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`questionId`	INTEGER NOT NULL,
	`response`	TEXT,
	`date`	NUMERIC,
	`userId`	TEXT NOT NULL
);

# --- !Downs
DROP TABLE Question;
DROP TABLE Answer;