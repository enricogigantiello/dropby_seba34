# --- !Ups

CREATE TABLE `Question` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`title`	TEXT,
	`detail`	TEXT,
	`date`	NUMERIC,
	`tags`	TEXT,
	`userId`	TEXT
);

# --- !Downs
DROP TABLE Question;