package controllers;

import models.Answer;
import models.Question;
import play.data.DynamicForm;
import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import play.db.jpa.*;

public class AllQuestions extends Controller {

    public static Result index (){
        return ok(views.html.index.render());
    }
    public static Result registerUser(){
        return ok(views.html.registration.render());
    }

    public static Result login(){
        return ok(views.html.login.render());
    }

    @play.db.jpa.Transactional(readOnly = true)
    public static Result showAll() {
        List<Question> allQuestions = Question.findAll();
        return ok(views.html.showAll.render(allQuestions));
    }

    @play.db.jpa.Transactional
    public static Result createQuestion(){
        DynamicForm form = Form.form().bindFromRequest();
        String quesTitle = form.get("title");
        String quesDetail = form.get("detail");
        String quesTags = form.get("tags");


        DateFormat dateFormat_1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        //DateFormat dateFormat_2 = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();

        String quesDate = dateFormat_1.format(date);
        //System.out.println(dateFormat_1.format(date));

        Question question = new Question();
        question.title = quesTitle;
        question.detail = quesDetail;
        question.tags = quesTags;
        question.date = quesDate;

        question.save();
        flash("success","Movie successful created");
        return redirect(controllers.routes.AllQuestions.addQuestion());
    }


    public static Result addQuestion() {
        return ok(views.html.addNewQuestion.render());
    }


    //send the title of the question to the template
    public static Result addResponse(Integer ques_id, String title) {

        //Question question = Question.findById(id);
        return ok(views.html.addNewResponse.render(ques_id,title));
    }

    @play.db.jpa.Transactional
    public static Result createResponse(Integer ques_id) {
        DynamicForm form = Form.form().bindFromRequest();
        String response = form.get("response");
        DateFormat dateFormat_1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        String ansDate = dateFormat_1.format(date);

        Answer ans = new Answer();
        ans.questionId = ques_id;
        ans.response = response;
        ans.userId = "amit";
        ans.date = ansDate;
        Question question = new Question();
        question.UpdateTotalAnswers(ques_id);
        question = null;


        ans.save();
        flash("success","Response successfully added");
         return redirect(controllers.routes.AllQuestions.showAll());
    }
}
