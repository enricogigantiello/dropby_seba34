package models;

import play.data.validation.Constraints;
import play.db.jpa.JPA;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.TypedQuery;
import java.util.List;


//import javax.persistence.Entity;
@Entity
public class Question {
    @Id
    @GeneratedValue
    public int id;

    public int totalAnswers;
    @Constraints.Required //for thowing errors
    public String title;
    public String detail;
    public String tags;
    public String userId;
    public String date;


    public void save() {
        JPA.em().persist(this);
    }

    public static List<Question> findAll(){
        TypedQuery<Question> query = JPA.em().createQuery("SELECT m FROM Question m",Question.class);
        return query.getResultList();
    }
//    public String findById(){
//        TypedQuery<Question> query = JPA.em().createQuery("SELECT m FROM Question m WHERE id = ''")
//    }
    public void UpdateTotalAnswers(Integer q_id){
        //Question question = Question.findById(q_id);
        Question question = JPA.em().find(Question.class,q_id);
        question.totalAnswers += 1;
        question.save();
    }
}
