package models;

import play.data.validation.Constraints;
import play.db.jpa.JPA;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


/**
 * Created by Amit Sama on 26-May-15.
 */
@Entity
public class Answer {
    @Id
    @GeneratedValue
    public int id;

    @Constraints.Required
    public int questionId;
    public String response;
    public String userId;
    public String date;

    public void save() { JPA.em().persist(this);}

}


